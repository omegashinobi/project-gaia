import manifest from '../../assets/manifest.json';

function init() {}

function load(scene : any) {

    let data : any;
    data = manifest;

    Object.keys(data).forEach((folder : string) => {
        scene.load.setBaseURL("../../assets/");
        switch (folder) {
            case "animatedSprites" : {
                Object.keys(data[folder]).forEach((dataSet : any) => {
                    scene.load.aseprite(dataSet, folder+"/"+data[folder][dataSet][1], folder+"/"+data[folder][dataSet][0]);
                });
                break;
            }
            case "tileMaps": {
                Object.keys(data[folder]).forEach((dataSet : any) => {
                    scene.load.multiatlas(dataSet, folder+"/"+data[folder][dataSet][0], folder);
                });
                break;
            }
            case "collision" : {
                Object.keys(data[folder]).forEach((dataSet : any) => {
                    scene.load.json(dataSet, folder+"/"+data[folder][dataSet][0],);
                });
                break;
            }
        }

    });
}

export default {
    load,
}