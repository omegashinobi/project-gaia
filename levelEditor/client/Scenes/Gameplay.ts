import * as Phaser from 'phaser';
import * as Game from "../index";

let config: Phaser.Types.Scenes.SettingsConfig = {
    key: 'gamePlay',
};

let gameObject: any = [];

export default class gameScene extends Phaser.Scene {

    constructor() {
        super(config);
    }

    public create() {

        Game.game.scene.start('ui');

        gameObject.forEach((e:any)=>{
            e.depth = e.y;
        });
    }
}