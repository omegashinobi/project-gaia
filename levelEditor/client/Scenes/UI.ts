import * as Phaser from 'phaser';
import {game} from "../index";

let config: Phaser.Types.Scenes.SettingsConfig = {
    key: 'ui',
};

let gameObject: any = [];

export default class gameScene extends Phaser.Scene {

    constructor() {
        super(config);
    }

    public create() {
        let graphics = this.add.graphics({ lineStyle: { width: 2, color: 0xeeeeee }, fillStyle: { color: 0xeeeeee } })

        gameObject[0] = new Phaser.Geom.Rectangle();
        gameObject[0].width = 50;
        gameObject[0].height = Number(game.config.height);

        gameObject[1] = new Phaser.Geom.Rectangle();
        gameObject[1].width = 50;
        gameObject[1].height = 10;

        gameObject[1].setPosition(150,5);

        graphics.fillRectShape(gameObject[0]);
        graphics.fillRectShape(gameObject[1]);


    }
}