import * as Phaser from 'phaser';
import loader from "../loader";
import * as Game from '../index';

let config: Phaser.Types.Scenes.SettingsConfig = {
    key: 'loader',
};

export default class gameScene extends Phaser.Scene {

    constructor() {
        super(config);
    }

    public preload() {
        loader.load(this);
    }

    public create() {
        Game.game.scene.start('gamePlay');
    }
}