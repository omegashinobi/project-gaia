import * as Phaser from 'phaser';
import Loader from './Scenes/Loader';
import Gameplay from './Scenes/Gameplay';

const gameConfig: Phaser.Types.Core.GameConfig = {
    title: 'Game',
    type: Phaser.AUTO,
    width: 320,
    height: 240,
    scale: {
        mode: Phaser.Scale.FIT,
        autoCenter: Phaser.Scale.CENTER_BOTH
    },
    physics: {
        default: 'matter',
        matter: {
            debug: true,
            gravity: {
                y: 0
            },
        },
    },
    render: {
        pixelArt: true
    },
    parent: 'game',
    backgroundColor: '#000000',

};

export const game = new Phaser.Game(gameConfig);

game.scene.add('loader',Loader);
game.scene.add('gamePlay',Gameplay);

game.scene.start('loader');


