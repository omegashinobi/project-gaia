import * as Phaser from 'phaser';
import MobController from './MobController'
import MobMovementController from "./MobMovementController";

export enum Direction {
    Up = 1,
    Down,
    Left,
    Right,
}

export enum MovementType {
    normal,
    velocity
}

export default class Mob extends Phaser.Physics.Matter.Sprite {
    mobTag : string;
    health : number;
    maxHealth : number;
    attackPower : number;
    weakness : [];
    controller : MobController;
    movement : MobMovementController;
    direction : Direction;
    moveType : any;
    moveSpeed : number;
    spriteSet : any;
    sprite : Phaser.GameObjects.Sprite;
    physics : Phaser.Physics.Matter.Sprite;
    distanceSensor : any

    constructor(props : any) {
        super(props.scene.matter.world,props.x,props.y,"");

        this.spriteSet = props.spriteSet;
        this.moveSpeed = 3;
        this.maxHealth = props.maxhealth;
        this.health = this.maxHealth;

        this.moveType = props.moveType || MovementType.normal

        this.sprite = new Phaser.GameObjects.Sprite(props.scene,0,0,"");

        this.direction = Direction.Down;

        this.mobTag = "none";

        this.controller = new MobController(this);
        this.movement = new MobMovementController(this,this.spriteSet);

        this.distanceSensor = this.scene.matter.add.circle(this.x,this.y, props.targetDistance || 32);
        this.distanceSensor.isSensor = true;

        this.scene.events.on('update',(time : any, delta : any) => {
            this.update(time,delta);
        }, this);

        this.setFixedRotation();
        this.distanceSensor.gameObject = this;

        props.scene.add.existing(this.sprite)
    }

    update(...args : any) {
        super.update(...args);
        this.sprite.setPosition(this.x,this.y);
        this.distanceSensor.position.x = this.x;
        this.distanceSensor.position.y = this.y;

        this.width = 2;
        this.sprite.depth = this.y;
    }
}