import Mob from "../Mob";
import movementController from './EnemyMovementController';

import {game} from '../../index';

export default class Enemy extends Mob {
    movementContoller: movementController
    target: any

    constructor(props: any) {
        super(props);

        this.target = undefined;
        this.moveSpeed = 1;

        this.mobTag = "Enemy";
        this.movementContoller = new movementController(this);

        this.setEvents();
    }

    setEvents() {
        let _this = this;
        this.scene.matter.world.on('collisionstart', function (event: any) {
            let pairs = event.pairs;

            for (let i = 0; i < pairs.length; i++) {
                let bodyA = pairs[i].bodyA;
                let bodyB = pairs[i].bodyB;

                if (pairs[i].isSensor) {
                    if (pairs[i].collision.bodyB.gameObject !== undefined) {
                        if (pairs[i].collision.bodyB.gameObject.mobTag === "Player") {
                            _this.movementContoller.setTarget(pairs[i].collision.bodyB.gameObject)
                        } else if (pairs[i].collision.bodyA.gameObject.mobTag === "Player") {
                            _this.movementContoller.setTarget(pairs[i].collision.bodyA.gameObject)
                        }
                    }
                }

            }
        });
    }

    update(...args: any) {
        super.update(...args);
        this.movementContoller.moveTowards();
    }
}