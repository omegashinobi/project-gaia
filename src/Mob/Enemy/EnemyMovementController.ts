import Enemy from "./Enemy";
import Mob, {MovementType} from "../Mob";

export default class enemyMovementController {
    mob: Enemy
    tween : any
    constructor(mob: Enemy) {
        this.mob = mob;

        let _this = this;
    }

    randMove() {
        setInterval(() => {
            let rand: number = Math.floor(Math.random() * 5) + 1;
            switch (rand) {
                case 1: {
                    this.mob.movement.moveUp();
                    break;
                }
                case 2: {
                    this.mob.movement.moveDown();
                    break;
                }
                case 3: {
                    this.mob.movement.moveLeft();
                    break;
                }
                case 4: {
                    this.mob.movement.moveRight();
                    break;
                }
                default: {
                    break;
                }
            }
        }, 1000)
    }

    moveTowards() {
        if(this.mob.target) {
            this.mob.moveType = MovementType.normal;

            this.mob.x = (1-this.mob.moveSpeed/100)*this.mob.x+this.mob.moveSpeed/100*this.mob.target.x;
            this.mob.y = (1-this.mob.moveSpeed/100)*this.mob.y+this.mob.moveSpeed/100*this.mob.target.y;

        }
    }

    setTarget(target : Mob) {
        this.mob.target = target;
        if(this.tween === undefined) {
            this.tween = this.mob.scene.time.delayedCall(5000,this.resetTarget,[],this);
        } else {
            this.tween.elapsed = 0;
        }

    }

    resetTarget() {
        this.mob.target = undefined;
        this.tween = undefined;
    }

}