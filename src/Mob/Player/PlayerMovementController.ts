import Player from "./Player";

export default class PlayerMovementController {
    player : Player
    cursors : any
    constructor(player : Player) {

        this.player = player;
        this.cursors = this.player.scene.input.keyboard.createCursorKeys();
    }

    public update() {
        if(this.cursors.up.isDown) {this.player.movement.moveUp();}
        if(this.cursors.down.isDown) {this.player.movement.moveDown();}
        if(this.cursors.left.isDown) {this.player.movement.moveLeft();}
        if(this.cursors.right.isDown) {this.player.movement.moveRight();}
    }
}