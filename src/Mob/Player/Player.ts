import Mob from "../Mob";
import movementController from './PlayerMovementController';

import {game} from '../../index';

export default class Player extends Mob {
    movementContoller: movementController
    inputDetection: any

    constructor(props: any) {
        super(props);

        this.mobTag = "Player";

        this.movementContoller = new movementController(this);

        this.scene.cameras.main.startFollow(this);

        this.inputDetection = {};
        this.inputDetection["space"] = this.scene.input.keyboard.addKey(Phaser.Input.Keyboard.KeyCodes.SPACE);

    }

    checkInput() {
        if (this.inputDetection["space"].isDown) {
            this.controller.attack();
        }
    }

    update(...args: any) {
        super.update(...args);
        this.movementContoller.update();
        this.checkInput()
    }
}