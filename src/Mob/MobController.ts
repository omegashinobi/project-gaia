import Mob from "./Mob";
import {Direction} from "./Mob";
import {game} from "../index"

export default class MobController {
    data: Mob
    attackBox: any
    isAttacking: boolean

    constructor(Model: Mob) {
        this.data = Model;
        this.isAttacking = false;
    }

    public attack() {
        if(!this.isAttacking) {
            this.isAttacking = true;
            this.attackBox = new Phaser.Physics.Matter.Sprite(this.data.scene.matter.world, 0, 0, "");

            this.attackBox.isSensor = true;
            this.attackBox.owner = this.data.mobTag;

            switch (this.data.direction) {
                case Direction.Up : {
                    this.attackBox.setPosition(this.data.x, this.data.y + -30);
                    break;
                }
                case Direction.Down : {
                    this.attackBox.setPosition(this.data.x, this.data.y + 30);
                    break;
                }
                case Direction.Right : {
                    this.attackBox.setPosition(this.data.x + 30, this.data.y);
                    break;
                }
                case Direction.Left : {
                    this.attackBox.setPosition(this.data.x + -30, this.data.y);
                    break;
                }
            }

            this.data.scene.tweens.add({
                targets: this.attackBox,
                ease: 'Power1',
                duration: 50,
                onComplete: () => {
                    this.attackBox.destroy();
                    this.isAttacking = false
                }
            });
        }
    }

    public dealDamage() {

    }

    public checkHealth() {

    }
}