import {Direction,MovementType} from "./Mob";
import Mob from "./Mob";

export default class MobMovementController {
    mob : Mob;
    adjustMobLoc : any;
    spriteSet : any;
    constructor(mob : Mob, spriteSet : any) {

        this.spriteSet = spriteSet;
        this.mob = mob;
        this.mob.scene.anims.createFromAseprite(spriteSet);

        this.adjustMobLoc = {
            x : (val:number)=>{
                if(this.mob.moveType === MovementType.normal) {
                    this.mob.x += val;
                    this.mob.sprite.x += val;
                } else {
                    this.mob.setVelocityX(val);
                }
            },
            y : (val:number)=>{
                if(this.mob.moveType === MovementType.normal) {
                    this.mob.y += val;
                    this.mob.sprite.y += val;
                }
                else {
                    this.mob.setVelocityY(val);
                }
            }
        }
        this.moveDown();
    }

    moveUp() {
        this.adjustMobLoc.y(-this.mob.moveSpeed);
        this.mob.direction = Direction.Up;
        this.mob.sprite.play({
            key: this.spriteSet+"Up",
            repeat: -1,
            duration:1
        });
    }

    moveDown() {
        this.adjustMobLoc.y(this.mob.moveSpeed);
        this.mob.direction = Direction.Down;
        this.mob.sprite.play({
            key: this.spriteSet+"Down",
            repeat: -1,
        });
    }

    moveLeft() {
        this.adjustMobLoc.x(-this.mob.moveSpeed);
        this.mob.direction = Direction.Left;
        this.mob.sprite.play({
            key: this.spriteSet+"Left",
            repeat: -1,
        });
    }

    moveRight() {
        this.adjustMobLoc.x(this.mob.moveSpeed);
        this.mob.direction = Direction.Right;
        this.mob.sprite.play({
            key: this.spriteSet+"Right",
            repeat: -1,
        });
    }
}