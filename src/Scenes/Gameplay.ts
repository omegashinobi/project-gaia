import * as Phaser from 'phaser';
import Player from "../Mob/Player/Player";
import Enemy from "../Mob/Enemy/Enemy";
import {MovementType} from "../Mob/Mob";

let config: Phaser.Types.Scenes.SettingsConfig = {
    key: 'gamePlay',
};

let gameObject: any = [];

export default class gameScene extends Phaser.Scene {

    constructor() {
        super(config);
    }

    public create() {

        console.log('gamePlay');
        let shapes : any = this.cache.json.get('swampCollisionMap');

        let player = new Player({
            scene: this,
            x: 200,
            y: 200,
            spriteSet:"player"
        });

        for(let i = 0; i< 1; i++) {
            new Enemy({
                scene: this,
                x: Math.floor(Math.random() * 300) + 10,
                y: Math.floor(Math.random() * 300) + 10,
                spriteSet:"swampSlime",
                moveType : MovementType.velocity,
                targetDistance: 128
            });
        }



        // @ts-ignore
        gameObject[0] = this.matter.add.sprite(100, 100, "swamp", "swamp house",{shape: shapes["swamp house"]});
        // @ts-ignore
        gameObject[1] = this.matter.add.sprite(200, 50, "swamp", "swamp house2",{shape: shapes["swamp house2"]});

        gameObject.forEach((e:any)=>{
            e.depth = e.y;
        });
    }
}