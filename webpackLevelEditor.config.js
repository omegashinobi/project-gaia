const path = require('path');
const { CleanWebpackPlugin } = require('clean-webpack-plugin/dist/clean-webpack-plugin');
const BrowserSyncPlugin = require('browser-sync-webpack-plugin');

module.exports = {
    entry: {
        app: [
            path.resolve(__dirname, 'levelEditor/client/index.ts')
        ],
    },
    mode: 'development',
    watch: true,
    module: {
        rules: [
            {
                test: /\.tsx?$/,
                use: 'ts-loader',
                exclude: /node_modules/,
            },
        ],
    },
    resolve: {
        extensions: ['.ts', '.tsx', '.js']
    },
    plugins: [
        new CleanWebpackPlugin(),
    ],
    output: {
        pathinfo: true,
        path: path.resolve(__dirname, 'levelEditor/dist'),
        publicPath: './levelEditor/dist/',
        filename: 'bundle.js'
    },
};