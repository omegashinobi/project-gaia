const {readdirSync, writeFileSync} = require('fs');
const path = require('path');

function build() {
    let data = {};

    console.log('building manifest....');

    const directories = source =>
        readdirSync(source, {withFileTypes: true})
            .filter(dirent => dirent.isDirectory())
            .map(dirent => dirent.name)

    directories(path.resolve(__dirname, './')).forEach(e => {
        data[e] = {};
        readdirSync(path.resolve(__dirname, './'+e)).forEach(file => {
            if(data[e][file.split('.')[0]] === undefined) {
                data[e][file.split('.')[0]] = [];
            }

            data[e][file.split('.')[0]].push(file);

        });
    });

    writeFileSync(path.resolve(__dirname, './' + 'manifest.json'), JSON.stringify(data))

}

build();